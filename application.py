#! /usr/bin/env python

import argparse
import os
import cv2
import numpy as np
from tqdm import tqdm
from preprocessing import parse_annotation
from utils import draw_boxes
from utils import heat
from frontend import YOLO
from flask import Flask,render_template, request
from pusher import Pusher
from keras import backend as K
import datetime
import json
import os, uuid, sys
from azure.storage.blob import BlockBlobService, PublicAccess
app = Flask(__name__)
os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"]="0"
pusher = Pusher(
app_id = "657181",
key = "b18933974129f4c2509b",
secret = "4fbef8028daa71805832",
cluster = "ap1",
ssl=True)

#@app.route('/pic', methods=['POST'])
#def pic():
#    data = request.form
#    pusher.trigger(u'pic',u'change',)
#    return "picc"

@app.route('/index')
def index():
    return render_template('index.html')

@app.route('/dashboard')
def dashboard():
    return render_template('dashboard.html')

@app.route('/orders', methods=['POST'])
def order():
    data = request.form
    pusher.trigger(u'order', u'place', {
        u'units': data['units']
    })
    return "units logged"

@app.route('/message', methods=['POST'])
def message():
    data = request.form
    pusher.trigger(u'message', u'send', {
        u'name': data['name'],
        u'message': data['message']
    })
    return "message sent"

@app.route('/customer', methods=['POST'])
def customer():
    data = request.form
    pusher.trigger(u'customer', u'add', {
        u'name': data['name'],
        u'position': data['position'],
    
        u'office': data['office'],
        u'age': data['age'],
        u'salary': data['salary'],
    })
    return "customer added"



@app.route("/predict")
def _main_():
    timestamp = datetime.datetime.now()
    K.clear_session()
    config_path  = 'config.json'
    weights_path = 'model.h5'
    image_path   = request.args.get('image')
    #image_path   = 'testImage.jpg'
    block_blob_service = BlockBlobService(account_name='facialdiag', account_key='Njme1TYYeaqVwLvmeXX6nqgPWNMkNuFmSVRkJgKw3kQn4otsg+II4gJqjkRTP0TgHUrCmKLkpsRGl6YNmWnTCQ==')
    container_name ='testimage'
    block_blob_service.get_blob_to_path(container_name, image_path, image_path) 
    with open(config_path) as config_buffer:    
        config = json.load(config_buffer)

    ###############################
    #   Make the model 
    ###############################

    yolo = YOLO(backend             = config['model']['backend'],
                input_size          = config['model']['input_size'], 
                labels              = config['model']['labels'], 
                max_box_per_image   = config['model']['max_box_per_image'],
                anchors             = config['model']['anchors'])

    ###############################
    #   Load trained weights
    ###############################    

    yolo.load_weights(weights_path)

    ###############################
    #   Predict bounding boxes 
    ###############################

    if image_path[-4:] == '.mp4':
        video_out = image_path[:-4] + '_detected' + image_path[-4:]
        video_reader = cv2.VideoCapture(image_path)

        nb_frames = int(video_reader.get(cv2.CAP_PROP_FRAME_COUNT))
        frame_h = int(video_reader.get(cv2.CAP_PROP_FRAME_HEIGHT))
        frame_w = int(video_reader.get(cv2.CAP_PROP_FRAME_WIDTH))

        video_writer = cv2.VideoWriter(video_out,
                               cv2.VideoWriter_fourcc(*'MPEG'), 
                               50.0, 
                               (frame_w, frame_h))

        for i in tqdm(range(nb_frames)):
            _, image = video_reader.read()
            
            boxes = yolo.predict(image)
            image = draw_boxes(image, boxes, config['model']['labels'])

            video_writer.write(np.uint8(image))

        video_reader.release()
        video_writer.release()  
    else:
        image = cv2.imread(image_path)
        boxes = yolo.predict(image)
        h = heat(image,boxes,image_path,timestamp)
        #image = draw_boxes(image, boxes, config['model']['labels'])
        #print(boxes[0].head)
        print(len(boxes), 'boxes are found')
		
        cv2.imwrite(image_path[:-4] + '_detected' + image_path[-4:], image)
    return "predict"+image_path+"done"
	
if __name__ == '__main__':
    app.run(debug=True)
    #_main_()
