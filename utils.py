import numpy as np
import os, uuid, sys
import xml.etree.ElementTree as ET
import tensorflow as tf
import copy
import cv2
from pyheatmap.heatmap import HeatMap
import math
from collections import OrderedDict
import csv
import datetime
import pandas as pd
from azure.storage.blob import BlockBlobService, PublicAccess
import pyodbc
from azure.storage.queue import QueueService
class BoundBox:
    def __init__(self, xmin, ymin, xmax, ymax, c = None, classes = None):
        self.xmin = xmin
        self.ymin = ymin
        self.xmax = xmax
        self.ymax = ymax
        
        self.c     = c
        self.classes = classes

        self.label = -1
        self.score = -1

    def get_label(self):
        if self.label == -1:
            self.label = np.argmax(self.classes)
        
        return self.label
    
    def get_score(self):
        if self.score == -1:
            self.score = self.classes[self.get_label()]
            
        return self.score

class WeightReader:
    def __init__(self, weight_file):
        self.offset = 4
        self.all_weights = np.fromfile(weight_file, dtype='float32')
        
    def read_bytes(self, size):
        self.offset = self.offset + size
        return self.all_weights[self.offset-size:self.offset]
    
    def reset(self):
        self.offset = 4

def bbox_iou(box1, box2):
    intersect_w = _interval_overlap([box1.xmin, box1.xmax], [box2.xmin, box2.xmax])
    intersect_h = _interval_overlap([box1.ymin, box1.ymax], [box2.ymin, box2.ymax])  
    
    intersect = intersect_w * intersect_h

    w1, h1 = box1.xmax-box1.xmin, box1.ymax-box1.ymin
    w2, h2 = box2.xmax-box2.xmin, box2.ymax-box2.ymin
    
    union = w1*h1 + w2*h2 - intersect
    
    return float(intersect) / union

def draw_boxes(image, boxes, labels):
    image_h, image_w, _ = image.shape

    for box in boxes:
        if (box.get_score()<0.5):
            continue
        xmin = int(box.xmin*image_w)
        ymin = int(box.ymin*image_h)
        xmax = int(box.xmax*image_w)
        ymax = int(box.ymax*image_h)

        cv2.rectangle(image, (xmin,ymin), (xmax,ymax), (0,255,0), 3)
        cv2.putText(image, 
                    labels[box.get_label()] + ' ' + str(box.get_score()), 
                    (xmin, ymin - 13), 
                    cv2.FONT_HERSHEY_SIMPLEX, 
                    1e-3 * image_h, 
                    (0,255,0), 2)
        
    return image  

def deduplicate(l):
    return list(uniq(l))

def uniq(lst):
    last = object()
    for item in lst:
        if item == last:
            continue
        yield item
        last = item
def is_zero_file(fpath):  
    return False if os.path.isfile(fpath) and os.path.getsize(fpath) > 0 else True
def heat(image, boxes, image_path,timestamp):
    image_h, image_w, _ = image.shape
    data = []
    sy = 0
    imagepath = image_path[:-4] + '_new' + image_path[-4:]
    cv2.imwrite(imagepath, image)
    hm = HeatMap(data, base = imagepath)
    hm.heatmap(save_as = imagepath,r = sy )
    #timestamp = datetime.datetime.now()
    print(timestamp)
    with open('head_detection.csv', mode='a') as csv_file:
        fieldnames = ['timestamp', 'x_min','x_max', 'y_min','y_max','x_avg','y_avg','radius','type','hour','minute']
        writer = csv.DictWriter(csv_file, lineterminator='\n', fieldnames=fieldnames)
        if ((is_zero_file('head_detection.csv'))):
            writer.writeheader()
        writer.writerow({'timestamp': timestamp, 'x_min': 0, 'x_max': 2000, 'y_min': 0, 'y_max': -2000
			 ,'x_avg': 0,'y_avg': 0,'radius':sy,'type':0,'hour':timestamp.hour,'minute':timestamp.minute})
        writer.writerow({'timestamp': timestamp, 'x_min': 0, 'x_max': 2000, 'y_min': 0, 'y_max': -2000
			 ,'x_avg': 0,'y_avg': -2000,'radius':sy,'type':0,'hour':timestamp.hour,'minute':timestamp.minute})
        writer.writerow({'timestamp': timestamp, 'x_min': 0, 'x_max': 2000, 'y_min': 0, 'y_max': -2000
			 ,'x_avg': -2000,'y_avg': 0,'radius':sy,'type':0,'hour':timestamp.hour,'minute':timestamp.minute})
        writer.writerow({'timestamp': timestamp, 'x_min': 0, 'x_max': 2000, 'y_min': 0, 'y_max': -2000
			 ,'x_avg': -2000,'y_avg': -2000,'radius':sy,'type':0,'hour':timestamp.hour,'minute':timestamp.minute})
    count = 0
    score = []	
    for box in boxes:
        #if (box.get_score()<0.5):
        #    continue
        count += 1 
        xmin = int(box.xmin*image_w)
        ymin = int(box.ymin*image_h)
        xmax = int(box.xmax*image_w)
        ymax = int(box.ymax*image_h)
        data.append([int((xmin+xmax)/2), int((ymin+ymax)/2)])
        #data.append([int((xmin+xmax+13)/2), int((ymin+ymax+13)/2)])
        sx = int((xmax - xmin)/2)
        sy = int((ymax - ymin)/2)
		
        score.append(box.get_score())
		
        with open('head_detection.csv', mode='a') as csv_file:
    #        fieldnames = ['timestamp', 'x_min','x_max', 'y_min','y_max','x_avg','y_avg','radius','type']
            writer = csv.DictWriter(csv_file, lineterminator='\n', fieldnames=fieldnames)
            csvreader = csv.reader(csv_file)
            #df = pd.read_csv('head_detection.csv', delim_whitespace=True)
			#print (csv_file)
    #        if ((is_zero_file('head_detection.csv'))):
    #            writer.writeheader()
            #csv_file.write({'timestamp': datetime.datetime.now(), 'x_min': xmin, 'x_max': xmax, 'y_min': ymin, 'y_max': ymax})
            writer.writerow({'timestamp': timestamp, 'x_min': xmin, 'x_max': xmax, 'y_min': ymin, 'y_max': ymax
			 ,'x_avg': int((xmin+xmax)/2),'y_avg': int((ymin+ymax)/2),'radius':sy,'type':1,'hour':timestamp.hour,'minute':timestamp.minute})
        #i += 1
        #for x in range(xmin, xmax):
        #    for y in range(ymin, ymax):
        #        data.append([x, y])
        #prevx = -11
        #prevy = -11
        #xmin = xmin + sx - sy
        #xmax = xmin + sy - sx 	
        #sx = sy

        for xx in range(xmin, xmax):
            for yy in range(ymin, ymax):
                if (True):	
                    x = xx - (xmin+sx)
                    y = yy - (ymin+sy)
                    #if(x<sx):
                     #   x = x*(-1)
                    #if(y<sy):
                    #    y = y*(-1)	
                    x = x / sx
                    y = y / sy
                    if ((y * y / 2 )<=1 and ( x * x / 2)<=1):
                        nx =(x * math.sqrt(1 - (y * y / 2)))
                        ny =( y * math.sqrt(1 - (x * x / 2)))
                        #x,y = (x * math.sqrt(1 - (y * y / 2))),( y * math.sqrt(1 - (x * x / 2)))
                        x = int(nx * sx)
                        y =	int(ny*sy)
                    #if(x<0):
                    #    x = x*(-1)
                    #if(y<0):
                    #    y = y*(-1)	
                    data.append([int(x+xmin+sx),int(y+ymin+sy)])    
                    
    print('a')
    hm = HeatMap(data, base = imagepath)
    r = int(sy)
    hm.heatmap(save_as = imagepath,r =r)
    data = []
  
    block_blob_service = BlockBlobService(account_name='facialdiag', account_key='Njme1TYYeaqVwLvmeXX6nqgPWNMkNuFmSVRkJgKw3kQn4otsg+II4gJqjkRTP0TgHUrCmKLkpsRGl6YNmWnTCQ==')

        # Create a container called 'quickstartblobs'.
    container_name ='heatimage'
    block_blob_service.create_container(container_name)

        # Set the permission so the blobs are public.
    block_blob_service.set_container_acl(container_name, public_access=PublicAccess.Container)

   
   
        # Write text to the file.
   # file = open(full_path_to_file,  'w')
   # file.write("Hello, World!")
   # file.close()



        # Upload the created file, use local_file_name for the blob name
    #local_file_name = str(timestamp.today().strftime('%Y-%m-%d')) +'_'+image_path
    local_file_name = str(timestamp.today()) +'_'+image_path
    block_blob_service.create_blob_from_path(container_name, local_file_name, imagepath)
    block_blob_service.create_blob_from_path(container_name, str("newimage.jpg"), imagepath)
    mean = np.mean(score)
    med = np.median(score)
    
    
    server = 'facial.database.windows.net'
    database = 'facial'
    username = 'tor'
    password = 'Napho12345678'
    driver= '{ODBC Driver 17 for SQL Server}'
    cnxn = pyodbc.connect('DRIVER='+driver+';SERVER='+server+';PORT=1433;DATABASE='+database+';UID='+username+';PWD='+ password)
    cursor = cnxn.cursor()
    #cursor.execute("INSERT INTO logs (TIMESTAMP,MEAN,MED,COUNT) VALUES ("+str(timestamp)+","+str(mean)+","+str(med)+","+str(count)+");")
    print(str(timestamp)+" "+str(mean)+" "+str(med)+" "+str(count))
    tsql = "INSERT INTO logs (TIMESTAMP,MEAN,MED,COUNT) VALUES (?,?,?,?);"
    with cursor.execute(tsql,str(timestamp),str(mean),str(med),str(count)):
        print ('Successfuly Inserted!')
    tsql = "SELECT * FROM logs;"
    with cursor.execute(tsql):
        row = cursor.fetchone()
        while row:
            print (str(row[0]) + " " + str(row[1]))
            row = cursor.fetchone()
    #row = cursor.fetchone()
    #while row:
    #    print (str(row[0]) + " " + str(row[1]))
    #    row = cursor.fetchone()
#    container_name ='logs'
#    block_blob_service.create_container(container_name)
#    block_blob_service.set_container_acl(container_name, public_access=PublicAccess.Container)
        # Create a file in Documents to test the upload and download.
#    local_path=os.path.expanduser("")
#    local_file_name = str(timestamp.today().strftime('%Y-%m-%d'))+"_log.txt"
        # Write text to the file.
#    full_path_to_file =os.path.join(local_path, local_file_name)
#    file = open(full_path_to_file,  'w')
#    file.write("mean, med, count\n")
#    file.write(str(mean)+", "+str(med)+", "+str(count)+"\n")
#    file.close()
	
#    block_blob_service.create_blob_from_path(container_name, local_file_name, str(timestamp.today().strftime('%Y-%m-%d'))+"_log.txt")

    # Download the blob(s).
        # Add '_DOWNLOADED' as prefix to '.txt' so you can see both files in Documents.
    #full_path_to_file2 = os.path.join(local_path, str.replace(image_path[:-4] ,'.jpg', '_DOWNLOADED.txt'))
 #  print("\nDownloading blob to " + full_path_to_file2)
 #   block_blob_service.get_blob_to_path(container_name, local_file_name, full_path_to_file2)
 # List the blobs in the container
 #   print("\nList blobs in the container")
 #   generator = block_blob_service.list_blobs(container_name)
 #   for blob in generator:
 #       print("\t Blob name: " + blob.name)
    queue_service = QueueService(account_name='facialdiag', account_key='Njme1TYYeaqVwLvmeXX6nqgPWNMkNuFmSVRkJgKw3kQn4otsg+II4gJqjkRTP0TgHUrCmKLkpsRGl6YNmWnTCQ==')
    queue_service.put_message('queue',local_file_name)
    return True
        
def decode_netout(netout, anchors, nb_class, obj_threshold=0.3, nms_threshold=0.3):
    grid_h, grid_w, nb_box = netout.shape[:3]

    boxes = []
    
    # decode the output by the network
    netout[..., 4]  = _sigmoid(netout[..., 4])
    netout[..., 5:] = netout[..., 4][..., np.newaxis] * _softmax(netout[..., 5:])
    netout[..., 5:] *= netout[..., 5:] > obj_threshold
    
    for row in range(grid_h):
        for col in range(grid_w):
            for b in range(nb_box):
                # from 4th element onwards are confidence and class classes
                classes = netout[row,col,b,5:]
                
                if np.sum(classes) > 0:
                    # first 4 elements are x, y, w, and h
                    x, y, w, h = netout[row,col,b,:4]

                    x = (col + _sigmoid(x)) / grid_w # center position, unit: image width
                    y = (row + _sigmoid(y)) / grid_h # center position, unit: image height
                    w = anchors[2 * b + 0] * np.exp(w) / grid_w # unit: image width
                    h = anchors[2 * b + 1] * np.exp(h) / grid_h # unit: image height
                    confidence = netout[row,col,b,4]
                    
                    box = BoundBox(x-w/2, y-h/2, x+w/2, y+h/2, confidence, classes)
                    
                    boxes.append(box)

    # suppress non-maximal boxes
    for c in range(nb_class):
        sorted_indices = list(reversed(np.argsort([box.classes[c] for box in boxes])))

        for i in range(len(sorted_indices)):
            index_i = sorted_indices[i]
            
            if boxes[index_i].classes[c] == 0: 
                continue
            else:
                for j in range(i+1, len(sorted_indices)):
                    index_j = sorted_indices[j]
                    
                    if bbox_iou(boxes[index_i], boxes[index_j]) >= nms_threshold:
                        boxes[index_j].classes[c] = 0
                        
    # remove the boxes which are less likely than a obj_threshold
    boxes = [box for box in boxes if box.get_score() > obj_threshold]
    
    return boxes    

def compute_overlap(a, b):
    """
    Code originally from https://github.com/rbgirshick/py-faster-rcnn.
    Parameters
    ----------
    a: (N, 4) ndarray of float
    b: (K, 4) ndarray of float
    Returns
    -------
    overlaps: (N, K) ndarray of overlap between boxes and query_boxes
    """
    area = (b[:, 2] - b[:, 0]) * (b[:, 3] - b[:, 1])

    iw = np.minimum(np.expand_dims(a[:, 2], axis=1), b[:, 2]) - np.maximum(np.expand_dims(a[:, 0], 1), b[:, 0])
    ih = np.minimum(np.expand_dims(a[:, 3], axis=1), b[:, 3]) - np.maximum(np.expand_dims(a[:, 1], 1), b[:, 1])

    iw = np.maximum(iw, 0)
    ih = np.maximum(ih, 0)

    ua = np.expand_dims((a[:, 2] - a[:, 0]) * (a[:, 3] - a[:, 1]), axis=1) + area - iw * ih

    ua = np.maximum(ua, np.finfo(float).eps)

    intersection = iw * ih

    return intersection / ua  
    
def compute_ap(recall, precision):
    """ Compute the average precision, given the recall and precision curves.
    Code originally from https://github.com/rbgirshick/py-faster-rcnn.

    # Arguments
        recall:    The recall curve (list).
        precision: The precision curve (list).
    # Returns
        The average precision as computed in py-faster-rcnn.
    """
    # correct AP calculation
    # first append sentinel values at the end
    mrec = np.concatenate(([0.], recall, [1.]))
    mpre = np.concatenate(([0.], precision, [0.]))

    # compute the precision envelope
    for i in range(mpre.size - 1, 0, -1):
        mpre[i - 1] = np.maximum(mpre[i - 1], mpre[i])

    # to calculate area under PR curve, look for points
    # where X axis (recall) changes value
    i = np.where(mrec[1:] != mrec[:-1])[0]

    # and sum (\Delta recall) * prec
    ap = np.sum((mrec[i + 1] - mrec[i]) * mpre[i + 1])
    return ap      
        
def _interval_overlap(interval_a, interval_b):
    x1, x2 = interval_a
    x3, x4 = interval_b

    if x3 < x1:
        if x4 < x1:
            return 0
        else:
            return min(x2,x4) - x1
    else:
        if x2 < x3:
             return 0
        else:
            return min(x2,x4) - x3          

def _sigmoid(x):
    return 1. / (1. + np.exp(-x))

def _softmax(x, axis=-1, t=-100.):
    x = x - np.max(x)
    
    if np.min(x) < t:
        x = x/np.min(x)*t
        
    e_x = np.exp(x)
    
    return e_x / e_x.sum(axis, keepdims=True)
