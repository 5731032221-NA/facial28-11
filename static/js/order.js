Chart.defaults.global.defaultFontFamily = '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';

Chart.defaults.global.defaultFontColor = '#292b2c';
/*
var ctx = document.getElementById("myBarChart");
var myLineChart = new Chart(ctx, {
  type: 'bar',
  data: {
	labels: ["-5", "-4", "-3", "-2", "-1", "0"],
	datasets: [{
	  label: "Revenue",
	  backgroundColor: "rgba(2,117,216,1)",
	  borderColor: "rgba(2,117,216,1)",
	  data: [0, 0,5, 0, 0, 0],
	}],
  },
  options: {
	scales: {
	  xAxes: [{
		time: {
		  unit: 'month'
		},
		gridLines: {
		  display: false
		},
		ticks: {
		  maxTicksLimit: 7
		}
	  }],
	},
	legend: {
	  display: false
	}
  }
});
*/
 var subject = ['Moe','Larry','Curly','Moe','Larry','Curly','Moe','Larry','Curly','Moe','Larry','Curly']
    var score = [1,6,2,8,2,9,4,5,1,5,2,8]

var data = [{
  type: 'scatter',
  x: subject,
  y: score,
  mode: 'markers',
  transforms: [{
    type: 'groupby',
    groups: subject,
    styles: [
      {target: 'Moe', value: {marker: {color: 'blue'}}},
      {target: 'Larry', value: {marker: {color: 'red'}}},
      {target: 'Curly', value: {marker: {color: 'black'}}}
    ]
  }]
}]

Plotly.plot('graph', data)
// Configure Pusher instance
const pusher = new Pusher('b18933974129f4c2509b', {
	cluster: 'ap1',
	encrypted: true
});

// Subscribe to poll trigger
var orderChannel = pusher.subscribe('order');

// Listen to 'order placed' event
var order = document.getElementById('order-count')
orderChannel.bind('place', function(data) {
  myLineChart.data.datasets.forEach((dataset) => {
	  dataset.data.fill(parseInt(data.units),-1);
  });
  myLineChart.update();
	order.innerText = parseInt(order.innerText)+6;
	
//var image = document.getElementsByClassName("image1");
var image = document.getElementsByID("imagena");
image.src = "https://www.google.co.th/images/branding/googlelogo/2x/googlelogo_color_272x92dp.png"
});